
#include <thread/pool.h>

namespace Thread {

// Optimal amount of threads is hw_concurrency -1
// leave 1 thread to doodle around for the rest..
Pool::Pool(Interface::Manager& manager)
: Pool(manager, std::max(1u, std::thread::hardware_concurrency() -1))
{
}

Pool::Pool(Interface::Manager& manager, size_t count)
: m_manager(manager)
{
    if(count == 0) 
    {
        count = 1;
    }
    m_threads.reserve(count);
    for(size_t i = 0; i < count; ++i) 
    {
        m_threads.emplace_back([this](){
            m_manager.run();
        });
    }
}

Pool::~Pool ()
{
    // Wait for all threads to stop
    close();
}

size_t Pool::size() const
{
    return m_threads.size();
}

void Pool::close()
{
    m_manager.close();
    for(auto& thread : m_threads) 
    {
        thread.join();
    }
    m_threads.clear();
}

} // ns Thread