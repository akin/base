
#ifndef BASE_LAMBDATASK_H_
#define BASE_LAMBDATASK_H_

#include <interface/task.h>
#include <functional>

namespace Base {

class LambdaTask : public Interface::Task
{
    BASE_ClassDef(LambdaTask);
private:
    const std::function<void()> m_func;
public:
    LambdaTask(const std::function<void()>& func);
    
    ~LambdaTask() final;

    void run() final;
};

} // ns Base

#endif // BASE_LAMBDATASK_H_
