
#ifndef BASE_MACROS_H_
#define BASE_MACROS_H_

#include <memory>

#define BASE_ClassDef(CTYPE) \
public: \
    using Shared = std::shared_ptr<CTYPE>;\
    using Weak = std::weak_ptr<CTYPE>

#endif // BASE_MACROS_H_
