
#ifndef BASE_TYPES_H_
#define BASE_TYPES_H_

#include <string>

namespace Base {

enum Access : uint8_t
{
    NONE = 0,
    READ = 1 << 0,
    WRITE = 1 << 1,
    APPEND = 1 << 2,
};

} // ns Base

#endif // BASE_TYPES_H_
