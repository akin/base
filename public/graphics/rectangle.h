
#ifndef BASE_GRAPHICS_RECTANGLE_H_
#define BASE_GRAPHICS_RECTANGLE_H_

#include <base/common.h>

namespace Graphics {

template <class Type>
class Rectangle
{
    BASE_ClassDef(Rectangle<Type>);
private:
    Type m_x;
    Type m_y;
    Type m_width;
    Type m_height;
public:
    Rectangle(Type x = 0, Type y = 0, Type width = 0, Type height = 0)
    : m_x(x)
    , m_y(y)
    , m_width(width)
    , m_height(height)
    {
    }

    Rectangle(const Rectangle<Type>& other)
    : m_x(other.m_x)
    , m_y(other.m_y)
    , m_width(other.m_width)
    , m_height(other.m_height)
    {
    }

    Type getX() const
    {
        return m_x;
    }

    void setX(Type val)
    {
        m_x = val;
    }

    Type getY() const
    {
        return m_y;
    }

    void setY(Type val)
    {
        m_y = val;
    }

    Type getX2() const
    {
        return m_x + m_width;
    }

    void setX2(Type val)
    {
        m_width = val - m_x;
    }

    Type getY2() const
    {
        return m_y + m_height;
    }

    void setY2(Type val)
    {
        m_height = val - m_y;
    }

    Type getWidth() const
    {
        return m_width;
    }

    void setWidth(Type val)
    {
        m_width = val;
    }

    Type getHeight() const
    {
        return m_height;
    }

    void setHeight(Type val)
    {
        m_height = val;
    }
};

} // ns Graphics

#endif // BASE_GRAPHICS_RECTANGLE_H_
