
#ifndef BASE_GRAPHICS_SAMPLER2D_H_
#define BASE_GRAPHICS_SAMPLER2D_H_

#include <base/common.h>
#include <graphics/texture2d.h>
#include <vector>

namespace Graphics {

template <class Type>
class Sampler2D
{
    BASE_ClassDef(Sampler2D<Type>);
private:
    Texture2D<Type>& m_texture;
public:
    virtual ~Sampler2D()
    {
    }

    Sampler2D(Texture2D<Type>& texture)
    : m_texture(texture)
    {
    }

    virtual Type& at(float x, float y)
    {
        auto width = m_texture.getWidth();
        auto height = m_texture.getHeight();

        // repetitive..
        x = std::fmod(x , width);
        y = std::fmod(y , height);

        if(x < 0)
        {
            x += width;
        }
        if(y < 0)
        {
            y += height;
        }

        size_t a = (size_t)x;
        size_t b = (size_t)y;

        return m_texture.at(a, b);
    }
};

} // ns Graphics

#endif // BASE_GRAPHICS_SAMPLER2D_H_
