
#ifndef BASE_GRAPHICS_TEXTURE2D_H_
#define BASE_GRAPHICS_TEXTURE2D_H_

#include <base/common.h>
#include <functional>
#include <graphics/buffer.h>

namespace Graphics {

template <class Type>
class Texture2D
{
    BASE_ClassDef(Texture2D<Type>);
private:
    size_t m_width = 0;
    size_t m_height = 0;

    Buffer<Type> m_buffer;
public:
    Texture2D();
    Texture2D(size_t width, size_t height);
    Texture2D(const Texture2D<Type>& other);
    template <class Type2> Texture2D(const Texture2D<Type2>& other);

    void resize(size_t width, size_t height);

    void clear();

    size_t getWidth() const;
    size_t getHeight() const;

    size_t sizeBytes() const;
    size_t size() const;

    const Buffer<Type>& getBuffer() const;

    const Type *data() const;
    Type *data();

    Type& at(size_t width, size_t height);

    void set(const Texture2D<Type>& other);
    template <class Type2> void set(const Texture2D<Type2>& other);
    
    void filter(const std::function<void(Type& pixel)>& func);
    void filter(const std::function<void(size_t x, size_t y, Type& pixel)>& func);
};

template <class T>
Texture2D<T>::Texture2D()
{
}

template <class T>
Texture2D<T>::Texture2D(size_t width, size_t height)
{
    resize(width, height);
}

template <class T>
Texture2D<T>::Texture2D(const Texture2D<T>& other)
{
    m_width = other.getWidth();
    m_height = other.getHeight();
    m_buffer.set(other.getBuffer());
}

template <class T>
template <class Type2>
Texture2D<T>::Texture2D(const Texture2D<Type2>& other)
{
    set<Type2>(other);
}

template <class T>
void Texture2D<T>::resize(size_t width, size_t height)
{
    m_width = width;
    m_height = height;
    m_buffer.resize(width * height);
}

template <class T>
void Texture2D<T>::clear()
{
    m_width = 0;
    m_height = 0;
    m_buffer.clear();
}

template <class T>
size_t Texture2D<T>::getWidth() const
{
    return m_width;
}

template <class T>
size_t Texture2D<T>::getHeight() const
{
    return m_height;
}

template <class T>
size_t Texture2D<T>::sizeBytes() const
{
    return m_buffer.size() * sizeof(T);
}

template <class T>
size_t Texture2D<T>::size() const
{
    return m_buffer.size();
}

template <class T>
const Buffer<T>& Texture2D<T>::getBuffer() const
{
    return m_buffer;
}

template <class T>
const T *Texture2D<T>::data() const
{
    return m_buffer.data();
}

template <class T>
T *Texture2D<T>::data()
{
    return m_buffer.data();
}

template <class T>
T& Texture2D<T>::at(size_t width, size_t height)
{
    assert(m_width > width || m_height > height);
    auto *data = m_buffer.data();
    return data[(height * m_width) + width];
}

template <class T>
void Texture2D<T>::set(const Texture2D<T>& other)
{
    m_width = other.getWidth();
    m_height = other.getHeight();
    m_buffer.set(other.getBuffer());
}

template <class T>
template <class Type2>
void Texture2D<T>::set(const Texture2D<Type2>& other)
{
    m_width = other.getWidth();
    m_height = other.getHeight();
    m_buffer.template set<Type2>(other.getBuffer());
}

template <class T>
void Texture2D<T>::filter(const std::function<void(T& pixel)>& func)
{
    auto target = data();
    size_t size = this->size();
    for(size_t i = 0 ; i < size ; ++i)
    {
        func(target[i]);
    }
}

template <class T>
void Texture2D<T>::filter(const std::function<void(size_t x, size_t y, T& pixel)>& func)
{
    auto target = data();
    size_t yoff = 0;
    for(size_t y = 0 ; y < m_height ; ++y)
    {
        yoff = y * m_width;
        for(size_t x = 0 ; x < m_width ; ++x)
        {
            func(x,y, target[yoff + x]);
        }
    }
}

} // ns Graphics

#endif // BASE_GRAPHICS_TEXTURE2D_H_
