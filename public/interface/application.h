
#ifndef BASE_APPLICATION_H_
#define BASE_APPLICATION_H_

#include <string>
#include <base/common.h>

namespace Interface {

class Application
{
    BASE_ClassDef(Application);
private:
    std::string m_name;
public:
    Application(const std::string& name = "");
    virtual ~Application() = 0;

    void parseArgs(int argc, char *argv[]);

    virtual bool init() = 0;

    virtual int run() = 0;
};

} // ns Interface

#endif // BASE_APPLICATION_H_