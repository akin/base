
#ifndef BASE_COMMAND_H_
#define BASE_COMMAND_H_

#include <base/common.h>

namespace Interface {

class Command {
    BASE_ClassDef(Command);
public:
    virtual ~Command() {}

    virtual void execute() = 0;
    virtual void undo() = 0;
};

} // ns Interface

#endif // BASE_COMMAND_H_
