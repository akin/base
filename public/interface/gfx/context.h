
#ifndef BASE_APPLICATION_H_
#define BASE_APPLICATION_H_

#include <string>
#include <base/common.h>

namespace Interface {
namespace Gfx {

class Context
{
    BASE_ClassDef(Context);
public:
    Context();
    virtual ~Context() = 0;

    virtual bool init() = 0;
    virtual void deinit() = 0;

    virtual void run() = 0;
};

} // ns Gfx
} // ns Interface

#endif // BASE_APPLICATION_H_