
#ifndef BASE_LOG_H_
#define BASE_LOG_H_

#include <string>
#include <base/common.h>

namespace Interface {

class Log {
    BASE_ClassDef(Log);
public:
    enum class Level : size_t {
		Message = 1,
		Warning = 16,
		Error = 32
	};
public:
    virtual ~Log() {}

	virtual void print(const char *file, size_t line, Level level, std::string message) = 0;
	virtual void print(Level level, std::string message) = 0;
};

} // ns Interface

#endif // BASE_LOG_H_
