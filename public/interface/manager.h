
#ifndef BASE_INTERFACE_MANAGER_H_
#define BASE_INTERFACE_MANAGER_H_

namespace Interface {

class Manager 
{
public:
    virtual ~Manager() {}

    virtual void run() = 0;
    virtual void close() = 0;
};

} // ns Interface

#endif // BASE_INTERFACE_MANAGER_H_