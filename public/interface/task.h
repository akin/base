
#ifndef BASE_TASK_H_
#define BASE_TASK_H_

#include <string>
#include <base/common.h>

namespace Interface {

class Task {
    BASE_ClassDef(Task);
public:
    virtual ~Task() {}

    virtual void run() = 0;
};

} // ns Interface

#endif // BASE_TASK_H_
