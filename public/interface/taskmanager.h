
#ifndef BASE_INTERFACE_TASKMANAGER_H_
#define BASE_INTERFACE_TASKMANAGER_H_

#include <interface/task.h>

namespace Interface {

class TaskManager 
{
    BASE_ClassDef(TaskManager);
public:
    virtual ~TaskManager() {}

    virtual void clear() = 0;
    virtual void add(Interface::Task::Shared work) = 0;
};

} // ns Interface

#endif // BASE_INTERFACE_TASKMANAGER_H_