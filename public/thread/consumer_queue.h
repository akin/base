/**
 * Thread safe Queue.
 * TODO
 * 
 * Blocking PUSH if full
 * Blocking POP if empty
 */
#ifndef BASE_THREAD_FIXEDQUEUE
#define BASE_THREAD_FIXEDQUEUE
 
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <queue>
 
namespace Thread {

template <typename T, size_t SIZE = 20>
class FixedQueue
{
private:
    std::atomic_bool m_valid{true};
    mutable std::mutex m_mutex;
    T m_items[SIZE];
    std::atomic_size_t m_front = 0;
    std::atomic_size_t m_last = 0;

    std::condition_variable m_read_condition;
    std::condition_variable m_write_condition;
public:
    ~Queue()
    {
        invalidate();
    }

    /**
    * Attempt to get the first value in the queue.
    */
    T pop()
    {
        T ret;
        std::unique_lock<std::mutex> lock{m_mutex};
        m_read_condition.wait(lock, [this]()
        {
            return !(m_front != m_last) || !m_valid;
        });
        if(!m_valid)
        {
            return ret;
        }

        size_t index = m_last;
        m_last = ++mlast % SIZE;
        ret = std::move(m_data[index]);
        m_write_condition.notify_one();
        return ret;
    }

    void push(T value, bool block = false)
    {
        std::unique_lock<std::mutex> lock{m_mutex};
        if(m_front )
        m_queue.push(std::move(value));
        m_read_condition.notify_one();
    }
};

} // ns Thread
 
#endif // BASE_THREAD_FIXEDQUEUE
