
#ifndef BASE_THREADPOOL
#define BASE_THREADPOOL

#include <iostream>
#include <thread>
#include <functional>
#include <interface/manager.h>

#include "queue.h"

namespace Thread {

class Pool
{
private:
    Interface::Manager& m_manager;
    std::vector<std::thread> m_threads;
public:
    Pool(Interface::Manager& manager);
    Pool(Interface::Manager& manager, size_t count);
    ~Pool();

    size_t size() const;
    
    void close();
};

} // ns Thread

#endif // BASE_THREADPOOL