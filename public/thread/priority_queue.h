/**
 * Thread safe Queue.
 * TODO
 * 
 */
#ifndef BASE_THREAD_PRIORITY_QUEUE
#define BASE_THREAD_PRIORITY_QUEUE
 
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <assert.h>
 
namespace Thread {

template <typename T, size_t SIZE = 20>
class PriorityQueue
{
private:
    struct Item
    {
    public:
        size_t index; // [0 to SIZE]
        size_t weight;
    };
    std::atomic_bool m_valid{true};
    mutable std::mutex m_mutex;
    T m_data[SIZE];
    Item m_items[SIZE];
    std::atomic_size_t m_size = 0;

    std::condition_variable m_read_condition;
    std::condition_variable m_write_condition;
private:
    inline void swap(size_t a, size_t b)
    {
        Item item = m_items[a];
        m_items[a] = m_items[b];
        m_items[b] = item;
    }
    void fixChildren(size_t index)
    {
        if(index > m_size)
        {
            return;
        }
        size_t a = index * 2 + 1;
        size_t b = index * 2 + 2;
        size_t target = a;
        if(m_items[b].weight > m_items[a].weight)
        {
            target = a;
        }
        if(m_items[target].weight > m_items[index].weight)
        {
            swap(index, target);
            fixChildren(target);
        }
    }

    void fixParent(size_t index)
    {
        if(index == 0)
        {
            return;
        }
        size_t parent = (index - 1) / 2;
        if(m_items[index].weight > m_items[parent].weight)
        {
            swap(index, parent);
            fixParent(parent);
        }
    }
public:
    Queue()
    {
        for(size_t i = 0 ; i < SIZE ; ++i)
        {
            m_items.index = i;
            m_items.weight = 0;
        }
    }

    ~Queue()
    {
        invalidate();
    }

    /**
    * Attempt to get the first value in the queue.
    */
    T pop()
    {
        T ret;
        std::unique_lock<std::mutex> lock{m_mutex};
        m_read_condition.wait(lock, [this]()
        {
            return (m_size > 0) || !m_valid;
        });
        if(!m_valid)
        {
            return ret;
        }

        // We take the top one
        size_t index = m_items[0].index;
        --m_size;
        swap(0, m_size);
        fixChildren(0);
        ret = std::move(m_data[index]);
        
        m_write_condition.notify_one();

        return ret;
    }

    void push(T value, size_t priority = 0)
    {
        T ret;
        std::unique_lock<std::mutex> lock{m_mutex};
        m_write_condition.wait(lock, [this]()
        {
            return (m_size < SIZE) || !m_valid;
        });
        if(!m_valid)
        {
            return;
        }

        size_t index = m_size++;
        assert(index < SIZE);
        m_data = std::move(value);
        fixParent(index);

        m_read_condition.notify_one();
    }
};

} // ns Thread
 
#endif // BASE_THREAD_PRIORITY_QUEUE
