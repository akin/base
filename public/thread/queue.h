/**
 * Thread safe Queue.
 */
#ifndef BASE_THREADQUEUE
#define BASE_THREADQUEUE
 
#include <atomic>
#include <condition_variable>
#include <mutex>
#include <queue>
 
namespace Thread {

template <typename T>
class Queue
{
private:
    std::atomic_bool m_valid{true};
    mutable std::mutex m_mutex;
    std::queue<T> m_queue;
    std::condition_variable m_read_condition;
public:
    ~Queue()
    {
        invalidate();
    }

    /**
    * Attempt to get the first value in the queue.
    */
    T pop(bool block = false)
    {
        T ret;
        std::unique_lock<std::mutex> lock{m_mutex};
        if(!block)
        {
            if(m_queue.empty() || !m_valid)
            {
                return ret;
            }
        }
        else
        {
            m_read_condition.wait(lock, [this]()
            {
                return !m_queue.empty() || !m_valid;
            });
            if(!m_valid)
            {
                return ret;
            }
        }

        ret = std::move(m_queue.front());
        m_queue.pop();
        return ret;
    }

    void push(T value)
    {
        std::lock_guard<std::mutex> lock{m_mutex};
        m_queue.push(std::move(value));
        m_read_condition.notify_one();
    }

    bool empty() const
    {
        std::lock_guard<std::mutex> lock{m_mutex};
        return m_queue.empty();
    }

    size_t size() const
    {
        std::lock_guard<std::mutex> lock{m_mutex};
        return m_queue.size();
    }

    void clear()
    {
        std::lock_guard<std::mutex> lock{m_mutex};
        while(!m_queue.empty())
        {
            m_queue.pop();
        }
        m_read_condition.notify_all();
    }

    /**
    * Invalidate the queue.
    * Used to ensure no conditions are being waited on in waitPop when
    * a thread or the application is trying to exit.
    * The queue is invalid after calling this method and it is an error
    * to continue using a queue after this method has been called.
    */
    void invalidate()
    {
        m_valid = false;
        m_read_condition.notify_all();
    }

    /**
    * Returns whether or not this queue is valid.
    */
    bool isValid() const
    {
        return m_valid;
    }
};

} // ns Thread
 
#endif // BASE_THREADQUEUE
