
option(TESTS_base "Add BASE Tests" OFF)
if(TESTS_base)
	set(TARGET_NAME base-tests)
	cmake_minimum_required(VERSION 3.7)

	set(TESTS)

	#list(APPEND TESTS "http_get")

	set(CURRENT_SOURCES)
	foreach(ITEM IN LISTS TESTS)
		list(APPEND CURRENT_SOURCES test.${ITEM}.cpp)
	endforeach()

	# add the catch implementation file
	list(APPEND CURRENT_SOURCES catch.cpp)

	add_executable(
		${TARGET_NAME} 
		${CURRENT_SOURCES}
	)

	target_link_libraries(
		${TARGET_NAME} 
		catch
		base
	)
endif()
